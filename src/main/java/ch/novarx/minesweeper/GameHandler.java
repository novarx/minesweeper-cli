package ch.novarx.minesweeper;

import static ch.novarx.minesweeper.GameState.LOOSE;
import static ch.novarx.minesweeper.GameState.PLAYING;

public class GameHandler {
    private final Area area;

    public GameHandler(Area area) {
        this.area = area;
    }

    public GameState checkWinCondition() {
        if (area.hasMineHit()) {
            return LOOSE;
        }
        return PLAYING;
    }
}
