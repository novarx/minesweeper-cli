package ch.novarx.minesweeper;


import java.util.List;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class Minesweeper {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        Area area = new Area(5, List.of(new Coordinates(3, 3)));
        System.out.println(area);
        while (s.hasNextLine()) {
            String line = s.nextLine();
            if (line.equals("exit")) {
                break;
            }
            var xy = line.split(",");
            if (xy[0] == null || xy[1] == null) continue;
            area.doCheck(parseInt(xy[0]), parseInt(xy[1]));
            System.out.println(area);
        }
    }
}
