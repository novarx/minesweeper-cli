package ch.novarx.minesweeper;

public enum GameState {
    PLAYING,
    VICTORY,
    LOOSE
}
