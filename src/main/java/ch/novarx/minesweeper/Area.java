package ch.novarx.minesweeper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class Area {
    private final List<List<Integer>> area;
    private final List<Coordinates> mines;

    public Area(int size, List<Coordinates> mines) {
        this.mines = mines;
        area = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            var line = new ArrayList<Integer>();
            area.add(line);
            for (int ii = 0; ii < size; ii++) {
                line.add(null);
            }
        }
    }

    public List<List<Integer>> getArea() {
        return area;
    }

    public Integer getState(int x, int y) {
        return area.get(x).get(y);
    }

    public void doCheck(int x, int y) {
        int bombsNearby = 0;

        if (mines.contains(new Coordinates(x, y))) {
            area.get(x).set(y, 9);
            return;
        }

        for (int xx = Math.max(x - 1, 0); xx <= Math.min(x + 1, area.size() - 1); xx++) {
            for (int yy = Math.max(y - 1, 0); yy <= Math.min(y + 1, area.size() - 1); yy++) {
                if (mines.contains(new Coordinates(xx, yy))) {
                    bombsNearby++;
                }
            }
        }

        area.get(x).set(y, bombsNearby);
    }

    @Override
    public String toString() {
        StringBuilder representation = new StringBuilder("  ");
        for (int i = 0; i < area.size(); i++) {
            representation.append(" ").append(i);
        }
        representation.append("\n");

        for (int i = 0; i < area.size(); i++) {
            List<Integer> line = area.get(i);
            representation.append(i).append(" ");
            for (var field : line) {
                representation.append(" ").append(field == null ? " " : field);
            }
            representation.append("\n");
        }

        return representation.toString();
    }

    public boolean hasMineHit() {
        return area.stream()
                .flatMap(Collection::stream)
                .filter(Objects::nonNull)
                .anyMatch(field -> field.equals(9));
    }
}
