package ch.novarx.minesweeper;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class AreaTest {
    Area sut;

    @BeforeEach
    void setup() {
        sut = new Area(5, List.of(
                new Coordinates(3, 4),
                new Coordinates(3, 3)
        ));
    }

    @Test
    void should_initialize_area() {
        assertThat(sut.getArea()).hasSize(5).allSatisfy(line -> {
            assertThat(line).hasSize(5).anySatisfy(item -> {
                assertThat(item).isEqualTo(null);
            });
        });
    }

    @Test
    void should_getStateOfField_unclicked() {
        assertThat(sut.getState(0, 0)).isEqualTo(null);
    }

    @Test
    void should_changeStateOfField_whenTested() {
        sut.doCheck(0, 0);
        assertThat(sut.getState(0, 0)).isEqualTo(0);
    }

    @Test
    void should_changeStateOfField_whenChecked_nearBomb() {
        sut.doCheck(4, 4);
        int state = sut.getState(4, 4);
        assertThat(state).isEqualTo(2);
    }

    @Test
    void should_changeStateOfField_whenChecked_onBomb() {
        sut.doCheck(3, 3);
        var state = sut.getState(3, 3);
        assertThat(state).isEqualTo(9);
    }

    @Test
    void should_getStringRepresentation() {
        assertThat(new Area(2, List.of())).hasToString(
                "" +
                        "   0 1\n" +
                        "0     \n" +
                        "1     \n"
        );
    }

    @Test
    void should_getStringRepresentation_nearMineChecked() {
        Area actual = new Area(2, List.of(new Coordinates(0, 0)));
        actual.doCheck(0, 1);
        assertThat(actual).hasToString(
                "" +
                        "   0 1\n" +
                        "0    1\n" +
                        "1     \n"
        );
    }

    @Nested
    class CoordinatesTest {
        @Test
        void should_initializeCoordinates() {
            var sut = new Coordinates(1, 2);
            assertThat(sut).isNotNull();
        }

        @Test
        void should_implement_equals() {
            var sut = new Coordinates(1, 2);
            var other = new Coordinates(1, 2);
            assertThat(sut).isEqualTo(other);
        }
    }

    @Test
    void should_checkForHitMine() {
        assertThat(sut.hasMineHit()).isFalse();
    }

    @Test
    void should_checkForHitMine__ifMineWasHit() {
        sut.doCheck(3, 3);
        assertThat(sut.hasMineHit()).isTrue();
    }
}
