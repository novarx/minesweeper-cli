package ch.novarx.minesweeper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static ch.novarx.minesweeper.GameState.LOOSE;
import static ch.novarx.minesweeper.GameState.PLAYING;
import static org.assertj.core.api.Assertions.assertThat;

class GameHandlerTest {
    GameHandler sut;
    Area area;

    @BeforeEach
    void setup() {
        area = new Area(3, List.of(new Coordinates(1, 1)));
        sut = new GameHandler(area);
    }

    @Test
    void should_checkWinCondition() {
        assertThat(sut.checkWinCondition()).isEqualTo(PLAYING);
    }

    @Test
    void should_checkWinCondition__ifBombIsHit() {
        area.doCheck(1, 1);
        assertThat(sut.checkWinCondition()).isEqualTo(LOOSE);
    }
}
